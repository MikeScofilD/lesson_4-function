<?php
namespace mathFunctions;
declare(ticks=1);
//Задача5:
//Есть файл - https://docs.google.com/spreadsheets/d/1bUlxpy2o6kKZO82QQeKy7tgw0qlV1nHd217zYMhN21A/edit?usp=sharing
// - в нем список функций, которые нужно использовать в дз;
//у каждого студента по 5 функций + 3 функции обязательные для всех;
//ваша задача написать пример (а лучше парочку) для каждой вашей функции, чтобы показать ее работу;
//все ваши функции и примеры напишите в отдельный файл с названием myFunctions.php и определите в отдельный неймспейс;
//Пример: у меня попалась функция coun(),
// я создаю несколько разных массивов,
// используя эту функцию показываю,
// как она работает - вывожу на экран сам массив и к-тво элементов в нем, подсчитанное этой функцией;
//каждый сделает свою часть и посмотрит эту же задачу у остальных,
// таким образом все узнают, как работает та или иная функция;
//просьба отнестись ответственно к этой задаче ибо от вашего выполнения зависит понимание остальных студентов.

class myFunctions {
    private $str;
    private $arr = [];
    public function __construct($str='Hello namespace myFunction !!!',$arr=["5" => "Abc","Хлеб" => 3, "Батон" => 10,"Колбаса" => 15,"size" => "XL", "color" => "gold","яблоко" => 31,"груша" => 15,"слива" => 32,"blue" => "yelow","персик" => 11]){
        $this->str = $str;
        $this->arr = $arr;
    }
    public function myFunctions(){
        echo $this->str;
        //substr_count() - Возвращает число вхождений подстроки (указанной строки).
        echo "</br>strlen = ".strlen($this->str)."</br>"; // 14
        echo "substr_count = ".substr_count($this->str, 'a')."</br>";
        echo "substr_count = ".substr_count($this->str, 'Hello')."</br>";
        echo "substr_count = ".substr_count($this->str, 'l')."</br>";
        echo "substr_count = ".substr_count($this->str, 'myFunction')."</br>";
        echo "substr_count = ".substr_count($this->str, '!')."</br>";

        //substr() - вырезает и возвращает подстроку из строки.
        echo "substr() = ".substr($this->str,0,5)."</br>";
        echo "substr() = ".substr($this->str,5,5)."</br>";
        echo "substr() = ".substr($this->str,10,5)."</br>";
        echo "substr() = ".substr($this->str,15,11)."</br>";

        //array_values() - Выбирает все значения массива.
        echo "array_values() = ";
            print_r(array_values($this->arr));
        echo "</br>";

        //asort() - Эта функция сортирует массив таким образом,
        // что сохраняются отношения между ключами и значениями.
        // Она полезна, в основном, при сортировке ассоциативных массивов,
        // когда важно сохранить отношение ключ =
        echo "asort() = Array {</br>";
        print_r(asort($this->arr));
        echo "</br>";
        foreach ($this->arr as $key => $val) {
            echo "$key = $val</br>";
        }
        echo "}</br>";

        //min() - Находит наименьшее значение.
        echo "min() = ".min($this->arr)."<br>";
        echo "max() = ".max($this->arr)."<br>";

        // time() - возвращает количество секунд, прошедших с начала Эпохи Unix
        echo 'Сейчас:           '. date('Y-m-d') ."</br>";
        $nextWeek = time() + (7 * 24 * 60 * 60);
        $nextMonth = time() + (30 * 24 * 60 * 60);
        echo 'Следующая неделя: '. date('Y-m-d', $nextWeek) ."</br>";
        echo 'Следующий месяц: '. date('Y-m-d', $nextMonth) ."</br>";
        echo "Наша строка = ".$this->str."</br>";
        //isset()
        echo "Проверка есть ли что то в строке ".isset($this->str)."</br>";
        //empty()
        echo "Проверка что строка пустая ".empty($this->str)."</br>";
    }
}